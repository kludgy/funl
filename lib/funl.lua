local funl = {}

------------------------------------------------------------------------------
--
-- FUNctional Lists
--
-- See accompanying README.md.
--
------------------------------------------------------------------------------

-- Nullary function given by g() := f(x1,x2,...xn)
--
-- This is simply a convenience over writing longhand functions,
-- ie: 'nullary(f, ...)' instead of 'function() return f(...) 
-- end'.
--
-- A less noisy dialect such as moonscript may obviate the
-- desire for nullary with syntax like, '() -> f(...)'.
--
function funl.nullary(f, ...)
    local a = {...}
    return function()
        return f(unpack(a))
    end
end

-- Iterable array values.
function funl.vals(a)
    -- Whoops. ipairs() does not behave like a custom iterator even though
    -- for-ipairs-do makes it look like this is the case! ipairs() returns a
    -- function r that enables random access into the array.
    return function()
        local n = #a
        local i = 0
        local r = ipairs(a)
        return function()
            local _,v = r(a, i)
            i = i + 1
            return v
        end
    end
end

-- Infinite sequence of x values.
function funl.cyclex(x)
    return function()
        return function()
            return x
        end
    end
end

-- Infinite cycles of xs.
function funl.cycle(xs)
    return function()
        local it = xs()
        return function()
            local x = it()
            if x == nil then
                it = xs()
                x = it()
            end
            return x
        end
    end
end

-- Sequence of numbers [a,b,..c] with step (b-a).
-- c == nil => infinite progression
-- Intractible c => empty sequence
-- a == b => infinite sequence of same value
function funl.nseq(a, b, c)
    return function()
        local x = a
        local s = b - a
        local k = 1
        if a > b then
            k = -1
        end
        return function()
            if c ~= nil and k*x > k*c then
                return nil
            end
            local y = x
            x = x + s
            return y
        end
    end
end

-- True iff f(x) for some x in xs. Halts on first thruthy.
function funl.any(f, xs)
    for x in xs() do
        if f(x) then
            return true
        end
    end
    return false
end

-- False iff not f(x) for some x in xs. Halts on first falsy.
function funl.all(f, xs)
    for x in xs() do
        if not f(x) then
            return false
        end
    end
    return true
end

-- First element.
function funl.head(xs)
    return xs()()
end

-- Everything after the first element.
function funl.tail(xs)
    return funl.drop(1, xs)
end

-- Everything but the last element.
function funl.init(xs)
    return function()
        local it = xs()
        local x = it()           -- Look ahead one iteration with x
        return function()
            local y = x
            x = it()
            if x == nil then
                return nil
            end
            return y
        end
    end
end

-- Last element.
function funl.last(xs)
    local y = nil
    for x in xs() do
        y = x
    end
    return y
end

-- First n elements.
function funl.take(n, xs)
    return function()
        local it = xs()
        local i = 1
        return function()
            if i > n then
                return nil
            end
            i = i + 1
            return it()
        end
    end
end

-- Prefix of xs satisfying f(x).
function funl.takeWhile(f, xs)
    return function()
        local it = xs()
        return function()
            local x = it()
            if f(x) then
                return x
            else
                return nil
            end
        end
    end
end

-- Everything but the first n elements.
function funl.drop(n, xs)
    return function()
        local it = xs()
        for i=1,n do
            it()
        end
        return function()
            return it()
        end
    end
end

-- Number of elements.
function funl.length(xs)
    local n = 0
    for x in xs() do
        n = n + 1
    end
    return n
end

-- i'th element.
function funl.at(xs, i)
    if i < 1 then
        return nil
    end
    local n = 1
    for x in xs() do
        if n == i then
            return x
        end
        n = n + 1
    end
    return nil
end

-- Shallow array evaluation. This returns an array object, not an iterator generator.
function funl.array(xs)
    local ys = {}
    for x in xs() do
        table.insert(ys, x)
    end
    return ys
end

-- Strict, shallow evaluation point. Use to capture expensive intermediate results.
function funl.strict(xs)
    return funl.vals(funl.array(xs))
end

-- Map x => f(x) over xs.
function funl.map(f, xs)
    return function()
        local it = xs()
        return function()
            local x = it()
            if x == nil then
                return nil
            else
                return f(x)
            end
        end
    end
end

-- Reverse the list.
function funl.reverse(xs)
    return function()
        -- Operate on a captured array for efficiency.
        local a = funl.array(xs)
        local i = #a
        return function()
            if i == 0 then
                return nil
            else
                local x = a[i]
                i = i - 1
                return x
            end
        end
    end
end

-- (x,y) => {x,y}
function funl.tuple(x, y)
    return {x, y}
end

-- Swap the arguments of a binary function.
function funl.swap(f)
    return function(y,x) return f(x,y) end
end

-- Composes xs,ys => [f(x1,y1),f(x2,y2)..f(xn,yn)].
function funl.zip(f, xs, ys)
    return function()
        local itx = xs()
        local ity = ys()
        return function()
            local x = itx()
            if x == nil then
                return nil
            end
            local y = ity()
            if y == nil then
                return nil
            end
            return f(x, y)
        end
    end
end

-- Left-fold where f(x,y) => x'.
function funl.foldl(f, z, ys)
    local x = z
    for y in ys() do
        x = f(x,y)
    end
    return x
end

-- Right-fold where f(y,x) => y'.
function funl.foldr(f, z, ys)
    return funl.foldl(f, z, funl.reverse(ys))
end

-- Elements satisfying f(x).
function funl.filter(f, xs)
    return function()
        local it = xs()
        return function()
            for x in it do
                if x == nil then
                    return nil
                elseif f(x) then
                    return x
                end
            end
            return nil
        end
    end
end

-- Append ys to xs.
function funl.append(xs, ys)
    return function()
        local kx = false
        local ky = false
        local it = nil
        local function page()
            if not kx then
                kx = true
                it = xs()
            elseif not ky then
                ky = true
                it = ys()
            end
        end
        page()
        return function()
            local x = it()
            if x == nil then
                page()
                x = it()
            end
            return x
        end
    end
end

-- Concatenate sublists.
function funl.concat(xss)
    return funl.foldl(funl.append, funl.vals{}, xss)
end

-- Empty list predicate.
function funl.null(xs)
    return funl.head(xs) == nil
end

-- Repeated applications of f to x, where iterate(f,x) => [x, f(x), f(f(x)), ...]
function funl.iterate(f, x)
    return function()
        local y = nil
        return function()
            if y ~= nil then
                y = f(y)
            else
                y = x
            end
            return y
        end
    end
end

-- Subdivision into regular lists of n elements.
function funl.paginate(n, xs)
    return function()
        local ys = xs
        return function()
            local zs = funl.take(n, ys)
            ys = funl.drop(n, ys)
            if funl.null(zs) then
                return nil
            else
                return zs
            end
        end
    end
end

-- Compose functions fs => f1(f2(...fn...)).
function funl.compose(fs)
    if funl.null(fs) then
        return nil
    end
    return function(...)
        local gs = funl.reverse(fs)
        return funl.foldl(
            function(x,g) return g(x) end,
            funl.head(gs)(...),
            funl.tail(gs))
    end
end

return funl
