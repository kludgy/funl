package = "funl"
version = "0.1.1-1"
source= {
	url = "https://bitbucket.org/kludgy/funl/get/v0.1.1-1.tar.gz",
	dir = "kludgy-funl-0e78deb40d71"
}


description = {
	summary = "FUNctional Lists for Lua",
	detailed = [[
		A set of functional list APIs based on Haskell's Prelude, implemented using 
		'iterable' generator functions.
	]],
	homepage = "https://bitbucket.org/Kludgy/funl",
	license = "MIT/X11",
	maintainer = "Darren Grant <dedgrant@gmail.com>"
}
dependencies = {
	"lua == 5.1",
}
build = {
	type = "builtin",
	modules =
	{
		funl = "lib/funl.lua"
	}
}
