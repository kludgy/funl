local _ = require("funl")

describe("funl", function()

    -- True iff finite lists are equivalent. Has the property that each list is only
    -- iterated through once per call. This is exploited below to verify that strict()
    -- works.
    local function equivlists(xs, ys)
        local itx = xs()
        local ity = ys()
        while true do
            local x = itx()
            local y = ity()
            if x == nil then
                if y == nil then
                    return true
                else
                    return false -- xs shorter than ys
                end
            elseif y == nil then
                if x == nil then
                    return true
                else
                    return false -- ys shorter than xs
                end
            elseif x ~= y then
                return false
            end
        end
    end

    -- Simple non-commutative binary function with a predictable result.
    local function sub(x, y)
        return x - y
    end

    describe("experiment", function()
        it("does stuff", function()
            local xs = _.nseq(1,2,10)
            local ys = _.nseq(1,2,10)
            assert.is_true(equivlists(xs, ys))
        end)
    end)
    describe("nullary", function()
        it("yields valid nullary function", function()
            local f = function(a,b) return a+b end
            local g = _.nullary(f, 1, 2)
            assert.is_equal(3, g())
        end)
    end)

    describe("vals", function()
        it("iterates correct array values", function()
            local a = {10,3,1,4,6,8}
            local i = 0
            local xs = _.vals(a)
            for x in xs() do
                i = i + 1
                assert.is_equal(a[i], x)
            end
            assert.is_equal(#a, i)
        end)
    end)

    describe("cyclex", function()
        it("seems to repeat the given value", function()
            local v = 12345
            local xs = _.take(100, _.cyclex(v))
            assert.is_true(_.all(function(x) return x == v end, xs))
        end)
    end)

    describe("cycle", function()
        it("seems to repeat the given list", function()
            local xs = _.vals{1,2,3,4,5,6,7,8,9,10}
            local ys = _.cycle(xs)
            for i=1,10 do
                assert.is_true(equivlists(xs, _.take(10, ys)))
                ys = _.drop(10, ys)
            end
        end)
        it("halts on empty list", function()
            assert.is_true(_.null(_.cycle(_.vals{})))
        end)
    end)

    describe("nseq", function()
        local xs = _.vals{1,2,3,4,5,6,7,8,9,10}
        it("yields correct finite list", function()
            local ys = _.nseq(1, 2, 10)
            assert.is_equal(10, _.length(ys))
            assert.is_true(equivlists(xs, ys))
        end)
        it("seems to produce infinite progression", function()
            local ys = _.nseq(1, 2)
            local i = 1
            for y in _.take(1000, ys)() do
                assert.is_equal(i, y)
                i = i + 1
            end
        end)
        it("seems to repeat constant value", function()
            local ys = _.nseq(123, 123)
            for y in _.take(1000, ys)() do
                assert.is_equal(123, y)
            end
        end)
        it("yields empty list for intractible end value", function()
            assert.is_true(_.null(_.nseq(1, 2, 0)))
            assert.is_true(_.null(_.nseq(2, 1, 3)))
            assert.is_true(_.null(_.nseq(1, 1, 0)))
        end)
        it("yields non-empty list for intractible ascending end value", function()
            assert.is_false(_.null(_.nseq(1, 1, 2)))
        end)
    end)

    describe("any", function()
        it("yields true on infinite list", function()
            assert.is_true(_.any(function(x) return x == 5 end, _.nseq(1, 2)))
        end)
        it("yields false on finite list", function()
            assert.is_false(_.any(function(x) return x == 11 end, _.nseq(1, 2, 10)))
        end)
    end)

    describe("all", function()
        it("yields true on finite list", function()
            assert.is_true(_.all(function(x) return x < 11 end, _.nseq(1, 2, 10)))
        end)
        it("yields false on infinite list", function()
            assert.is_false(_.all(function(x) return x ~= 5 end, _.nseq(1, 2)))
        end)
    end)

    describe("head", function()
        it("yields nil for empty list", function()
            assert.is_nil(_.head(_.vals{}))
        end)
        it("yields first value", function()
            assert.is_equal(345, _.head(_.vals{345,123,678}))
        end)
    end)

    describe("tail", function()
        it("propagates empty list", function()
            assert.is_true(_.null(_.tail(_.vals{})))
        end)
        it("yields empty list for singleton", function()
            assert.is_true(_.null(_.tail(_.vals{1})))
        end)
        it("yields all but first value", function()
            local xs = _.vals{1,2,3,4,5,6,7,8,9,10}
            assert.is_true(equivlists(_.tail(xs), _.drop(1, xs)))
        end)
    end)

    describe("init", function()
        it("propagates empty list", function()
            assert.is_true(_.null(_.init(_.vals{})))
        end)
        it("yields all but last value", function()
            local xs = _.vals{1,2,3,4,5,6,7,8,9,10}
            assert.is_true(equivlists(_.init(xs), _.take(9, xs)))
        end)
    end)

    describe("last", function()
        it("yields nil for empty list", function()
            assert.is_nil(_.last(_.vals{}))
        end)
        it("yields head for singleton", function()
            assert.is_equal(1, _.last(_.vals{1}))
        end)
    end)

    describe("take", function()
        local xs = _.nseq(1, 2)
        it("propagates empty list", function()
            assert.is_true(_.null(_.take(100000, _.vals{})))
        end)
        it("yields first n values from infinite list", function()
            assert.is_true(equivlists(_.take(5, _.nseq(1, 2)), _.nseq(1, 2, 5)))
        end)
        it("halts at end of list", function()
            assert.is_true(equivlists(_.take(100000, _.nseq(1, 2, 5)), _.nseq(1, 2, 5)))
        end)
    end)

    describe("takeWhile", function()
        local xs = _.nseq(1, 2, 10)
        local t = function() return true end
        it("propagates empty list", function()
            assert.is_true(_.null(_.takeWhile(t, _.vals{})))
        end)
        it("propagates entire list", function()
            assert.is_true(equivlists(_.takeWhile(t, xs), xs))
        end)
        it("yields expected prefix", function()
            assert.is_true(equivlists(_.takeWhile(function(x) return x ~= 6 end, xs), _.nseq(1, 2, 5)))
        end)
    end)

    describe("drop", function()
        local xs = _.vals{1,2,3,4,5,6,7,8,9,10}
        it("yields all but first n values", function()
            local ys = _.drop(5, xs)
            assert.is_equal(5, _.length(ys))
            local i = 6
            for y in ys() do
                assert.is_equal(i, y)
                i = i + 1
            end
        end)
        it("does not change list for n < 1", function()
            local ys = _.drop(0, xs)
            assert.is_equal(10, _.length(ys))
            local i = 1
            for y in ys() do
                assert.is_equal(i, y)
                i = i + 1
            end
        end)
        it("halts at end of list", function()
            local ys = _.drop(11, xs)
            assert.is_true(_.null(ys))
        end)
    end)

    describe("length", function()
        it("yields the expected length", function()
            assert.is_equal(100, _.length(_.nseq(1,2,100)))
        end)
    end)

    describe("at", function()
        local xs = _.nseq(1,2,10)
        it("yields expected value at index", function()
            for i=1,10 do
                assert.is_equal(i, _.at(xs, i))
            end
        end)
        it("yields nil for index out of range", function()
            assert.is_nil(_.at(xs, -1))
            assert.is_nil(_.at(xs, 0))
            assert.is_nil(_.at(xs, 11))
        end)
    end)

    describe("array", function()
        it("yields equivalent array object from list", function()
            local xs = _.nseq(1, 2, 10)
            local a = _.array(xs)
            assert.is_equal(_.length(xs), #a)
            for i=1,#a do
                assert.is_equal(_.at(xs, i), a[i])
            end
        end)
    end)

    describe("strict", function()
        it("does not re-evaluate list", function()
            local i = 0
            local function xs()
                local j = 0
                i = i + 1
                return function()
                    if j >= 3 then
                        return nil
                    end
                    j = j + 1
                    return j + i
                end
            end
            -- Verify that the iterable mutates list elements each time through.
            assert.is_true(equivlists(xs, _.vals{2,3,4}))
            assert.is_true(equivlists(xs, _.vals{3,4,5}))
            -- And verify that strict prevents further mutation.
            local ys = _.strict(xs)
            assert.is_true(equivlists(ys, _.vals{4,5,6}))
            assert.is_true(equivlists(ys, _.vals{4,5,6}))
        end)
    end)

    describe("map", function()
        local function f(x)
            return 2*x
        end
        local xs = _.map(f, _.vals{1,2,3})
        it("yields list of same length", function()
            assert.is_equal(3, _.length(xs))
        end)
        it("yields expected transform", function()
            assert.is_true(equivlists(xs, _.vals{2,4,6}))
        end)
    end)

    describe("reverse", function()
        it("propagates empty list", function()
            assert.is_true(_.null(_.reverse(_.vals{})))
        end)
        it("yields an expected result", function()
            assert.is_true(equivlists(_.nseq(1000,999,1), _.reverse(_.nseq(1,2,1000))))
        end)
        it("yields identity when applied twice", function()
            local xs = _.nseq(1,2,1000)
            assert.is_true(equivlists(xs, _.reverse(_.reverse(xs))))
        end)
    end)

    describe("tuple", function()
        it("generates valid tuple", function()
            assert.is_true(equivlists(_.vals{1,2}, _.vals(_.tuple(1,2))))
        end)
        it("ignores additional values (only supports 2-tuples)", function()
            assert.is_true(equivlists(_.vals{1,2}, _.vals(_.tuple(1,2,3,4,5))))
        end)
    end)

    describe("swap", function()
        it("yields result of swapped parameters when applied once", function()
            assert.is_equal(sub(2, 1), _.swap(sub)(1, 2))
        end)
        it("yields identity when applied twice", function()
            assert.is_equal(sub(1, 2), _.swap(_.swap(sub))(1, 2))
        end)
        it("discards additional parameters", function()
            local function f(a,b,c,d) 
                c = c or 3
                d = d or 4
                return a-b-c-d 
            end
            local g = _.swap(f)
            assert.is_equal(f(1,2), g(2,1))
        end)
    end)

    describe("zip", function()
        it("halts at the shorter input", function()
            assert.is_true(_.null(_.zip(sub, _.vals{}, _.vals{1})))
            assert.is_true(_.null(_.zip(sub, _.vals{1}, _.vals{})))
        end)
        it("applies custom zip function", function()
            assert.is_true(equivlists(_.zip(sub, _.vals{2,5,10}, _.vals{1,2,3}), _.vals{1,3,7}))
        end)
    end)

    describe("foldl", function()
        it("yields identity for empty list", function()
            assert.is_equal(1000, _.foldl(sub, 1000, _.vals{}))
        end)
        it("produces expected result for given left-values", function()
            assert.is_equal(990, _.foldl(sub, 1000, _.vals{1,2,3,4}))
        end)
        it("terminates on infinite list", function()
            assert.is_equal(990, _.foldl(sub, 1000, _.take(4,_.nseq(1,2))))
        end)
    end)

    describe("foldr", function()
        it("yields identity for empty list", function()
            assert.is_equal(1000, _.foldr(sub, 1000, _.vals{}))
        end)
        it("produces expected result for given right-values", function()
            assert.is_equal(990, _.foldr(sub, 1000, _.vals{1,2,3,4}))
        end)
    end)

    describe("filter", function()
        local xs = _.nseq(1,2,10)
        it("matches all from start", function()
            local ys = _.filter(function(x) return x < 6 end, xs)
            assert.is_true(equivlists(ys, _.nseq(1,2,5)))
        end)
        it("matches all from middle", function()
            local ys = _.filter(function(x) return x > 4 and x < 8 end, xs)
            assert.is_true(equivlists(ys, _.nseq(5,6,7)))
        end)
        it("matches all from end", function()
            local ys = _.filter(function(x) return x > 5 end, xs)
            assert.is_true(equivlists(ys, _.nseq(6,7,10)))
        end)
    end)

    describe("append", function()
        local xs = _.nseq(1,2,10)
        local ys = _.nseq(11,12,20)
        it("produces concatenation of two lists", function()
            assert.is_true(equivlists(_.append(xs, ys), _.nseq(1,2,20)))
        end)
        it("handles empty left list correctly", function()
            assert.is_true(equivlists(_.append(_.vals{}, ys), ys))
        end)
        it("handles empty right list correctly", function()
            assert.is_true(equivlists(_.append(xs, _.vals{}), xs))
        end)
    end)

    describe("concat", function()
        it("yields expected concatenation of sublists", function()
            assert.is_true(equivlists(_.concat(
                _.vals{_.vals{1,2,3}, _.vals{4,5,6}, _.vals{7,8,9}}),
                _.nseq(1,2,9)))
        end)
    end)

    describe("null", function()
        it("yields true on empty list", function()
            assert.is_true(_.null(_.vals{}))
        end)
        it("yields false on infinite list", function()
            assert.is_false(_.null(_.nseq(1,2)))
        end)
    end)

    describe("iterate", function()
        local function addone(x)
            return x + 1
        end
        it("yields initial value as first element", function()
            assert.is_equal(123, _.head(_.iterate(addone, 123)))
        end)
        it("yields expected applications", function()
            assert.is_true(equivlists(_.take(10, _.iterate(addone, 123)), _.nseq(123,124,132)))
        end)
    end)

    describe("paginate", function()
        it("yields expected sublists when divided evenly", function()
            local xss = _.paginate(2,_.nseq(1,2,4))
            assert.is_equal(2, _.length(xss))
            for xs in xss() do
                assert.is_equal(2, _.length(xs))
            end
            assert.is_true(equivlists(_.concat(xss), _.nseq(1,2,4)))
        end)
        it("yields expected sublists when there is a remainder", function()
            local xss = _.paginate(3,_.nseq(1,2,4))
            assert.is_equal(2, _.length(xss))
            local ys = _.at(xss, 1)
            local zs = _.at(xss, 2)
            assert.is_equal(3, _.length(ys))
            assert.is_equal(1, _.length(zs))
            assert.is_true(equivlists(_.concat(xss), _.nseq(1,2,4)))
        end)
        it("propagates empty list when n < 1", function()
            assert.is_true(_.null(_.paginate(0, _.nseq(1,2,10))))
            assert.is_true(_.null(_.paginate(-1, _.nseq(1,2,10))))
        end)
        it("propagates entire list when n exceeds length", function()
            local xs = _.nseq(1,2,10)
            local yss = _.paginate(100, xs)
            assert.is_equal(1, _.length(yss))
            assert.is_true(equivlists(_.head(yss), xs))
        end)
    end)

    describe("compose", function()
        local function double(x)
            return 2*x
        end
        it("yields nil for empty list", function()
            assert.is_nil(_.compose(_.vals{}))
        end)
        it("appears to yield identity as a single value", function()
            assert.is_equal(4, _.compose(_.vals{double})(2))
        end)
        it("yields expected result from a composition", function()
            assert.is_equal(8, _.compose(_.take(2, _.cyclex(double)))(2))
            assert.is_equal(16, _.compose(_.take(3, _.cyclex(double)))(2))
        end)
    end)

end)