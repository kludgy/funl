FUNctional Lists
================

(funl is pronounced "funnel")


Functions in funl draw direct inspiration from Haskell's Prelude, rather than the specifically purpose-driven operations defined in libraries based on underscore.js. Why reinvent something that works well in a functional context? Expect composability to be the priority rather than efficiency. The liberal application of closures and intermediate objects in funl may make the library unviable to some users of Lua, but I hope that its benefits will outweigh the costs.


Here's how it works:

- Every algorithm in funl is based on an iterable object, that is a nullary function who generates a new iterator on each call.

- Any scalar result is returned as-is, and any indeterminate result is returned as a new iterable. The abstraction is semantically analogous to IEnumerable in C# (especially through LINQ) or lists in Haskell.

- Generated iterators form a *non-strict* evaluation context. However, an iterable may cache values internally to prevent redundant computations by inserting funl.strict()!

- All iterations will halt at nil. This aggregates atomically to algorithms such as zip, where if any component is unavailable, then the whole element is considered to be nil.


No examples are available yet, although the Busted tests in spec/funl.spec should be a good place to start.
